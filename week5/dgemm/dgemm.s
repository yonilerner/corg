	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_dgemm
	.align	4, 0x90
_dgemm:                                 ## @dgemm
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
Ltmp3:
	.cfi_offset %rbx, -56
Ltmp4:
	.cfi_offset %r12, -48
Ltmp5:
	.cfi_offset %r13, -40
Ltmp6:
	.cfi_offset %r14, -32
Ltmp7:
	.cfi_offset %r15, -24
	movq	%rdx, -48(%rbp)         ## 8-byte Spill
	movq	%rsi, -72(%rbp)         ## 8-byte Spill
                                        ## kill: EDI<def> EDI<kill> RDI<def>
	testl	%edi, %edi
	jle	LBB0_11
## BB#1:                                ## %.preheader.lr.ph.split.us
	movq	%rdi, -56(%rbp)         ## 8-byte Spill
	movslq	%edi, %r9
	leal	-1(%rdi), %r8d
	leaq	(%r9,%r9), %rax
	leal	1(%rdi), %edx
	movl	%edx, -76(%rbp)         ## 4-byte Spill
	xorl	%r11d, %r11d
	movq	-72(%rbp), %rdx         ## 8-byte Reload
	movq	%rdx, -64(%rbp)         ## 8-byte Spill
	.align	4, 0x90
LBB0_2:                                 ## %.lr.ph4.split.us.us
                                        ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB0_4 Depth 2
                                        ##       Child Loop BB0_9 Depth 3
	movl	$1, %r13d
	xorl	%r15d, %r15d
	.align	4, 0x90
LBB0_4:                                 ## %.lr.ph.us.us
                                        ##   Parent Loop BB0_2 Depth=1
                                        ## =>  This Loop Header: Depth=2
                                        ##       Child Loop BB0_9 Depth 3
	movq	%r15, %rdx
	imulq	%r9, %rdx
	leaq	(%rdx,%r11), %r12
	movsd	(%rcx,%r12,8), %xmm0    ## xmm0 = mem[0],zero
	movq	-56(%rbp), %rsi         ## 8-byte Reload
	testb	$1, %sil
	je	LBB0_5
## BB#6:                                ##   in Loop: Header=BB0_4 Depth=2
	movq	-72(%rbp), %rsi         ## 8-byte Reload
	movsd	(%rsi,%r11,8), %xmm1    ## xmm1 = mem[0],zero
	movq	-48(%rbp), %rsi         ## 8-byte Reload
	mulsd	(%rsi,%rdx,8), %xmm1
	addsd	%xmm1, %xmm0
	movl	$1, %ebx
	jmp	LBB0_7
	.align	4, 0x90
LBB0_5:                                 ##   in Loop: Header=BB0_4 Depth=2
	xorl	%ebx, %ebx
LBB0_7:                                 ## %.lr.ph.us.us.split
                                        ##   in Loop: Header=BB0_4 Depth=2
	testl	%r8d, %r8d
	je	LBB0_3
## BB#8:                                ## %.lr.ph.us.us.split.split
                                        ##   in Loop: Header=BB0_4 Depth=2
	leaq	(%rbx,%r13), %rdx
	movq	-48(%rbp), %rsi         ## 8-byte Reload
	leaq	(%rsi,%rdx,8), %r10
	movq	%r9, %rdx
	imulq	%rbx, %rdx
	movq	-64(%rbp), %rdi         ## 8-byte Reload
	leaq	(%rdi,%rdx,8), %rsi
	leaq	1(%rbx), %rdx
	movl	-76(%rbp), %r14d        ## 4-byte Reload
	subl	%edx, %r14d
	imulq	%r9, %rdx
	leaq	(%rdi,%rdx,8), %rdx
	xorl	%edi, %edi
	.align	4, 0x90
LBB0_9:                                 ##   Parent Loop BB0_2 Depth=1
                                        ##     Parent Loop BB0_4 Depth=2
                                        ## =>    This Inner Loop Header: Depth=3
	movsd	(%rsi,%rdi,8), %xmm1    ## xmm1 = mem[0],zero
	mulsd	-8(%r10), %xmm1
	addsd	%xmm0, %xmm1
	movsd	(%rdx,%rdi,8), %xmm0    ## xmm0 = mem[0],zero
	mulsd	(%r10), %xmm0
	addsd	%xmm1, %xmm0
	addq	$2, %rbx
	addq	$16, %r10
	addq	%rax, %rdi
	addl	$-2, %r14d
	jne	LBB0_9
LBB0_3:                                 ##   in Loop: Header=BB0_4 Depth=2
	movsd	%xmm0, (%rcx,%r12,8)
	addq	%r9, %r13
	cmpl	%r8d, %r15d
	leaq	1(%r15), %r15
	jne	LBB0_4
## BB#10:                               ##   in Loop: Header=BB0_2 Depth=1
	addq	$8, -64(%rbp)           ## 8-byte Folded Spill
	cmpl	%r8d, %r11d
	leaq	1(%r11), %r11
	jne	LBB0_2
LBB0_11:                                ## %._crit_edge9
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc


.subsections_via_symbols
