// This belongs to Laivi
#include<stdio.h>
#include<time.h>
#include<stdlib.h>

#define SEED 1227
#define ASIZE 1024
#define MULTIPLIER 10.0
#define HOWMANYTOPRINT 12

void arrayMultiplier (int n, double *A, double *B, double *C);
int arrayFiller (double *A, double *B);
void printArray (double *C, double cpuTime);

int main (void){
    int fullSize = ASIZE * ASIZE;
    double *A, *B, *C;

    A = malloc(sizeof(double) * (fullSize));
    B = malloc(sizeof(double) * (fullSize));
    C = malloc(sizeof(double) * (fullSize));
    clock_t start, end;
    double cpu_time_used;

    arrayFiller(A,B);
    start = clock();
    arrayMultiplier(ASIZE, A, B, C);
    end = clock();
    cpu_time_used = ((double) (end - start));
    printArray(C, cpu_time_used);
    return 0;
}

void arrayMultiplier (int n, double *A, double *B, double *C) 
{
  int i, j, k;
  for ( i = 0; i < n; ++i)
    {
        for ( j = 0; j < n; ++j) 
        {
            double cij = C[i +j * n];
            for ( k = 0; k < n; k++)
            {
                cij +=  A[i + k * n] * B[k + j * n];
            }
            C[i + j * n] = cij;
        }
    } 
 }

int arrayFiller (double *A, double *B) {
    int i = 0;
    srand(SEED);
    for (i=0; i < ASIZE * ASIZE; i++)
        A[i] = ((float)rand()/(float)(RAND_MAX)) * MULTIPLIER;
    for (i=0; i < ASIZE * ASIZE; i++)
        B[i] = ((float)rand()/(float)(RAND_MAX)) * MULTIPLIER;
    return 0;
}

void printArray (double *C, double cpuTime){
    int i=0;
    for(i=0; i<HOWMANYTOPRINT; i++)
        printf("%lf \n", C[i]);
    printf("This multiplication took %lf clock cycles!", cpuTime);
}
