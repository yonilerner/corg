#include <x86intrin.h>
#include "x86intrin.h"
#include <immintrin.h>
#define UNROLL (4)

void dgemm(int n, double* A, double* B, double* C)
{
  __m256d c[4] __attribute__((aligned (32)));
  __m256d b __attribute__((aligned (32)));
  int i, j, x, k;
  for ( i = 0; i < n; i+=UNROLL*4 )
    for ( j = 0; j < n; j++ ) {
      for ( x = 0; x < UNROLL; x++ )
        c[x] = _mm256_load_pd(C+i+x*4+j*n);

      for( k = 0; k < n; k++ )
      {
        b = _mm256_broadcast_sd(B+k+j*n);
        for (x = 0; x < UNROLL; x++)
          c[x] = _mm256_add_pd(c[x],
            _mm256_mul_pd(_mm256_load_pd(A+n*k+x*4+i), b));
      }

      for ( x = 0; x < UNROLL; x++ )
        _mm256_store_pd(C+i+x*4+j*n, c[x]);
    }
}
