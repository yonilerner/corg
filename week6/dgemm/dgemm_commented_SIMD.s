	.file	"dgemm_SIMD.c"
	.text
	.globl	dgemm
	.type	dgemm, @function
dgemm:
.LFB742:
	.cfi_startproc
	pushq	%rbp # push quad word onto 64-bit (r is 64, e is 32) base pointer
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp #move quad word from stack pointer to base pointer
	.cfi_def_cfa_register 6
	andq	$-32, %rsp #and quad words -32 and rsp
	subq	$152, %rsp #subtract quad word 152 from rsp and store in rsp
	movl	%edi, -92(%rsp) #move long word from general purpose destination register to 92 from rsp
	movq	%rsi, -104(%rsp) #move quad from general purpose source register to 104 from rsp
	movq	%rdx, -112(%rsp) #rdx is data register (general purpose)
	movq	%rcx, -120(%rsp) #rcx is counter register (general purpose)
	movl	$0, -84(%rsp)
	jmp	.L2 # jump to another location in code
.L12:
	movl	$0, -80(%rsp)
	jmp	.L3
.L11:
	movl	-84(%rsp), %eax #eax is general purpose accumulator register
	movslq	%eax, %rdx #move sign extended word to a double word register
	movl	-80(%rsp), %eax
	imull	-92(%rsp), %eax #signed multiplication of integers
	cltq # convert 32-bit int stored in eax to a 64-bit int stored in rax
	addq	%rdx, %rax #add quad
	leaq	0(,%rax,8), %rdx #load effective address
	movq	-120(%rsp), %rax
	addq	%rdx, %rax #again, if eax is 32-bit accumulator, then rax is 64-bit version of the same
	movq	%rax, -72(%rsp)
	movq	-72(%rsp), %rax
  vmovapd	(%rax), %ymm0 # move aligned packed double precision float point value. ymm# is a 256-bit register
	vmovapd	%ymm0, -40(%rsp)
	movl	$0, -76(%rsp)
	jmp	.L5
.L10:
	movl	-76(%rsp), %eax
	movslq	%eax, %rdx
	movl	-80(%rsp), %eax
	imull	-92(%rsp), %eax
	cltq
	addq	%rdx, %rax
	leaq	0(,%rax,8), %rdx
	movq	-112(%rsp), %rax
	addq	%rdx, %rax
	movq	%rax, -64(%rsp)
	movq	-64(%rsp), %rax
	vbroadcastsd	(%rax), %ymm0 #broadcast floating point data
	movl	-84(%rsp), %eax
	movslq	%eax, %rdx
	movl	-76(%rsp), %eax
	imull	-92(%rsp), %eax
	cltq
	addq	%rdx, %rax
	leaq	0(,%rax,8), %rdx
	movq	-104(%rsp), %rax
	addq	%rdx, %rax
	movq	%rax, -56(%rsp)
	movq	-56(%rsp), %rax
	vmovapd	(%rax), %ymm1
	vmovapd	%ymm1, -8(%rsp)
	vmovapd	%ymm0, 24(%rsp)
	vmovapd	24(%rsp), %ymm0
	vmovapd	-8(%rsp), %ymm1
	vmulpd	%ymm0, %ymm1, %ymm0 #multiply packed double precision floating point values
	vmovapd	-40(%rsp), %ymm1
	vmovapd	%ymm1, 56(%rsp)
	vmovapd	%ymm0, 88(%rsp)
	vmovapd	88(%rsp), %ymm0
	vmovapd	56(%rsp), %ymm1
	vaddpd	%ymm0, %ymm1, %ymm0 #add packed double precision floating point values
	vmovapd	%ymm0, -40(%rsp)
	addl	$1, -76(%rsp)
.L5:
	movl	-76(%rsp), %eax
	cmpl	-92(%rsp), %eax #compare two long values
	jl	.L10
	movl	-84(%rsp), %eax
	movslq	%eax, %rdx
	movl	-80(%rsp), %eax
	imull	-92(%rsp), %eax
	cltq
	addq	%rdx, %rax
	leaq	0(,%rax,8), %rdx
	movq	-120(%rsp), %rax
	addq	%rdx, %rax
	movq	%rax, -48(%rsp)
	vmovapd	-40(%rsp), %ymm0
	vmovapd	%ymm0, 120(%rsp)
	movq	-48(%rsp), %rax
	vmovapd	120(%rsp), %ymm0
	vmovapd	%ymm0, (%rax)
	addl	$1, -80(%rsp)
.L3:
	movl	-80(%rsp), %eax
	cmpl	-92(%rsp), %eax
	jl	.L11 #jump if less than
	addl	$4, -84(%rsp) #add long word
.L2:
	movl	-84(%rsp), %eax
	cmpl	-92(%rsp), %eax
	jl	.L12
	leave #destroy the stack frame
	.cfi_def_cfa 7, 8
	ret #return from the function call and pop the last value off the stack
	.cfi_endproc
.LFE742:
	.size	dgemm, .-dgemm
	.ident	"GCC: (Ubuntu 4.8.4-2ubuntu1~14.04) 4.8.4"
	.section	.note.GNU-stack,"",@progbits
