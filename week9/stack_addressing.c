#include <stdio.h>

void test2(void) {
  int j = 2;
  printf("%p\n", &j);
}

void test1(void) {
  int i = 1;
  printf("%p\n", &i);
  test2();
}

int main() {
  test1();
}
