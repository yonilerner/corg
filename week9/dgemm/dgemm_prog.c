#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <immintrin.h>
#include "x86intrin.h"

double get_rand(void) {
  return ((double)rand()/(double)(RAND_MAX));
}

int usage(void) {
    printf("This program displays the runtime and result of an nxn square matrix.\n");
    printf("Usage: dgemm [n] [-print] [-norandom] [-help]\n");
    printf("    n should be the size for an nxn square matrix. DO NOT MAKE n TOO BIG.\n");
    printf("    -print is an optional argument. If present the input and output matrices will be printed.\n");
    printf("    -norandom uses a specific seed to randomly generate the input matrices.\n");
    printf("    -help prints this message and exists.\n");
    return 1;
}

void dgemm(int n, double *A, double *B, double *C);

#define RAND_SEED 1227
#define DEFAULT_SIZE 1024
#define VALUES_TO_PRINT 20

int main(int argc, char **argv) {
  if (argc > 4) {
    return usage();
  }
  int i;
  clock_t start, end;
  double cpu_time_used;

  int n = DEFAULT_SIZE;
  int do_print = 0;
  int use_random = 1;

  for (i = 1; i < argc; i++) {
    int temp_int = atoi(argv[i]);
    if (temp_int <= 0) {
      if (strcmp(argv[i], "-print") == 0) {
        do_print = 1;
      }
      if (strcmp(argv[i], "-norandom") == 0) {
        use_random = 0;
      }
      if (strcmp(argv[i], "-help") == 0) {
        usage();
        return 0;
      }
    } else {
      n = temp_int;
    }
  }

  if (use_random) {
    srand(RAND_SEED);
  }


  double *A, *B, *C;
  A = _mm_malloc(sizeof(double) * (n*n), 64);
  B = _mm_malloc(sizeof(double) * (n*n), 64);
  C = _mm_malloc(sizeof(double) * (n*n), 64);

  printf("Memory for input and output matrices has been allocated, beginning initialization.\n");

  int condition;
  for (i = 0; i < (condition = (n*n)); i++) {
    double a_val = (use_random ? get_rand() : (double) i);
    double b_val = (use_random ? get_rand() : (double) i);
    A[i] = a_val;
    B[i] = b_val;
    C[i] = 0.0;
    if (do_print) {
      if(i != 0 && i % n == 0) {
        printf("\n");
      }
      printf("(%.2lf, %.2lf) ", a_val, b_val);
      if (i + 1 >= condition) {
        printf("\n");
      }
    }
  }

  printf("Matrices have been allocated and initialized. Beginning multiplication.\n");

  start = clock();
  dgemm(n, A, B, C);
  end = clock();

  cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
  printf("The elapsed cpu time is %lf\n", cpu_time_used);

  if (do_print) {
    for (i = 0; i < n*n; i++) {
      if(i != 0 && i % n == 0) {
        printf("\n");
      }
      printf("%.2lf ", C[i]);
    }
    printf("\n");
  }

  printf("The first %d values in the resulting matrix are: ", VALUES_TO_PRINT);
  for (i = 0; i < VALUES_TO_PRINT; i++) {
    printf("%.2lf ", C[i]);
  }
  printf("\n");

  _mm_free(A);
  _mm_free(B);
  _mm_free(C);

  return 0;
}
