#define BLOCKSIZE 32
void do_block (int n, int si, int sj, int sk, double *A, double *B, double *C)
{
  int i, j, k;
  double cij;
  for (i = si; i < si+BLOCKSIZE; ++i)
    for (j = sj; j < sj+BLOCKSIZE; ++j)
    {
      cij = C[i+j*n];/* cij = C[i][j] */
      for( k = sk; k < sk+BLOCKSIZE; k++ )
        cij += A[i+k*n] * B[k+j*n];/* cij+=A[i][k]*B[k][j] */
      C[i+j*n] = cij;/* C[i][j] = cij */
    }
}

void dgemm (int n, double* A, double* B, double* C)
{
  int sj, si, sk;
  for ( sj = 0; sj < n; sj += BLOCKSIZE )
    for ( si = 0; si < n; si += BLOCKSIZE )
      for ( sk = 0; sk < n; sk += BLOCKSIZE )
        do_block(n, si, sj, sk, A, B, C);
}
