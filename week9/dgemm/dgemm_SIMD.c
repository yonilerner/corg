#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include "x86intrin.h"
#include <immintrin.h>


void dgemm(int n, double* A, double* B, double* C)
{
  int i, j, k;
  __m256d c0 __attribute__((aligned (32)));
  for (  i = 0; i < n; i+=4)
    for (  j = 0; j < n; j++) {
      //if (1016 >= i) printf("OK with i = %d, j= %d, k=%d\n",i,j,k);
      c0 = _mm256_load_pd(C+i+j*n); /* c0 = C[i][j] */
      for(  k=0; k < n; k++ )
        //if (1020 >= i) printf("OK with i = %d, j= %d, k=%d\n",i,j,k);
        c0 = _mm256_add_pd(c0, /* c0 += A[i][k]*B[k][j] */
          _mm256_mul_pd(_mm256_load_pd(A+i+k*n),
          _mm256_broadcast_sd(B+k+j*n)));
      _mm256_store_pd(C+i+j*n, c0); /* C[i][j] c0 */
    }
}
