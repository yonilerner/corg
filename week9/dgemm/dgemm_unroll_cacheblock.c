#include <x86intrin.h>
#include "x86intrin.h"
#include <immintrin.h>
#define UNROLL (8)
#define BLOCKSIZE 32

void dgemm_unroll(int n, double* A, double* B, double* C, int si, int sj, int sk)
{
  __m256d c[4] __attribute__((aligned (32)));
  __m256d b __attribute__((aligned (32)));
  int i, j, x, k;
  for ( i = si; i < si+BLOCKSIZE; i+=UNROLL*4 )
    for ( j = sj; j < sj+BLOCKSIZE; j++ ) {
      for ( x = 0; x < UNROLL; x++ )
        c[x] = _mm256_load_pd(C+i+x*4+j*n);

      for( k = sk; k < sk+BLOCKSIZE; k++ )
      {
        b = _mm256_broadcast_sd(B+k+j*n);
        for (x = 0; x < UNROLL; x++)
          c[x] = _mm256_add_pd(c[x],
            _mm256_mul_pd(_mm256_load_pd(A+n*k+x*4+i), b));
      }

      for ( x = 0; x < UNROLL; x++ )
        _mm256_store_pd(C+i+x*4+j*n, c[x]);
    }
}
void dgemm(int n, double* A, double* B, double* C)
{
  int sj, si, sk;
  for ( sj = 0; sj < n; sj += BLOCKSIZE )
    for ( si = 0; si < n; si += BLOCKSIZE )
      for ( sk = 0; sk < n; sk += BLOCKSIZE )
        dgemm_unroll(n, A, B, C, si, sj, sk);
}
