#include <stdio.h>

__asm__(
  "printf_str:\n\t"
  ".asciz \"%p\n\""
);

void test2(void) {
  __asm__(
    "movq %rbp, %rsi\n\t"
    "leaq  printf_str(%rip), %rdi\n\t"
    "movb $0, %al\n\t"
    "callq _printf"
  );
}

void test1(void) {
  __asm__(
    "movq %rbp, %rsi\n\t"
    "leaq  printf_str(%rip), %rdi\n\t"
    "movb $0, %al\n\t"
    "callq _printf\n\t"
  );
  test2();
}

int main() {
  test1();
}
