#include<stdio.h>
/* remember to compile with -fopenmp */

int main()
{
   int a, b, c, i;


#pragma omp parallel
  {
    // Code inside this region runs in parallel.

  for (i = 0; i<100000000; i++)
    if (((i+1)%99999999) == 0)
      printf("Hello! How many printouts do I give?\n");
  }


   printf("Now how Many?\n");

   return 0;
}
