/*masterprog.c */
#include <stdio.h>
#include <unistd.h>

int main()
{
    if(fork() != 0)
    {
    printf("From the parent:\n");
    execl ("first", "first.c", "-1", (char *)0);
    }
  else 
    {
    printf("From the child:\n");
    execl ("second", "second.c", "-1", (char *)0);
    }
  return 0;  
}